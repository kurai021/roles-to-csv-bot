//const dotenv = require('dotenv');
//dotenv.config();

const Discord = require('discord.js');
const roleToCSV = require('./helpers/roleToCSV');

const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES", 'GUILD_MEMBERS', "GUILD_PRESENCES"] })
const prefix = "!"

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
})

client.on('messageCreate', message => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length)
    const args = commandBody.trim().split(/ +/g)
    const command = args.shift().toLowerCase()
    const argsMerged = args.join(' ')

    if(command === 'tocsv') {

        if(message.channel.name.includes(`${process.env.CHANNEL}`) || message.channel.name.includes(`${process.env.OPTCHANNEL}`)) {
            if(!args.length){
                message.reply("You did not specify a role!")
            }
    
            else {
                roleToCSV.init(message, argsMerged)
            }
        }
        else {
            message.channel.send(`You must be in ${process.env.CHANNEL} or ${process.env.OPTCHANNEL} channel to use this command!`)
        }
    }
})

client.login(process.env.BOT_TOKEN);