const { MessageAttachment } = require('discord.js');
const { parseAsync } = require('json2csv');

function init(message,role){
    console.log(`${message.author.username} requested to export ${role}`)

    let findRole = message.guild.roles.cache.find(x => x.name === role);
   
    if (findRole === undefined) {
        message.reply("That role does not exist!")
    } 
    else {
        const roleList = message.guild.members.cache.filter(member => member.roles.cache.find(role => role == findRole)).map(member => member.user.tag);
        let parsedRoleList = [];
        const fields = ['ID'];
        const opts = { fields };

        for(const i in roleList){
            parsedRoleList.push({ ID: roleList[i] })
        }

        //console.log(parsedRoleList)

        parseAsync(parsedRoleList, opts)
            .then(csv => {
                const attachment = new MessageAttachment(Buffer.from(csv), 'role.csv');
                message.reply({ files: [attachment] });
            })
            .catch(err => {
                console.error(err);
            });
    }
}

module.exports = {
    init:init
}